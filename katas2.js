console.log("Katas1 add")
function add(a, b) {
  return a + b
}
console.log(add(6, 8))
// expected output: 14

console.log("Katas2 add")
function multiply(value, times) {
  let total = 0
  for (let i = 0; i < times; i++) {
    total = add(value, total)
  }
  return total
}
console.log(multiply(5, 5))
// expected output: 48

console.log("Katas3 add")
function power(value, exponent) {
  let total = 1
  for (let i = 0; i < exponent; i++) {
    total = multiply(value, total)
  }
  return total
}
console.log(power(2, 3))
// expected output: 8

console.log("Katas4 add")
function factorial(number) {
  let myfactorial = number
  for (let i = number-1; i > 0; i--) {
    myfactorial = multiply(myfactorial, i)
    // console.log (i)
  }
  return myfactorial
}
console.log(factorial(4))
// expected output: 24